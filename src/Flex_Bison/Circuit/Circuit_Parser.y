%{
#include <stdio.h>

extern "C" int yylex();
extern "C" int yyparse();
//extern "C" FILE *yyin();
%}


%union{
	char *sval;
	int ival;
}

%token <sval> ID_CIRCUITO
%token <sval> VAR
%token <sval> START
%token <sval> END
%token <sval> SMICOL
%token <sval> UNKNOW
%token <ival> N_INT

%%

circuito:
	ID_CIRCUITO START componentes END
	;
componentes:
	componentes componente
	| componente
	;
componente:
	variaveis ID_CIRCUITO variaveis SMICOL
	;
variaveis:
	variaveis variavel
	| variavel
	;
variavel:
	VAR
	;
	

%%

int main(){
	yylex();
}
//http://aquamentus.com/flex_bison.html

//fazer pushs em variaveis 