%{
# include<stdio.h>
%}

%%
[A-Z][a-zA-Z0-9]*	{printf("Nome_Circuito ");} //{yylval.sval = strdup(yytext);return ID_CIRCUITO;} 
[0-9]*				{printf("N_INT ");} // {yylval.ival = atoi(yytext);return N_INT;}
[a-z][a-z0-9]*		{printf("ID_VAR ");} //{yylval.sval = strdup(yytext);return VAR;}
\{					{printf("START ");}   // {return START;}
\}					{printf("END ");}	  //{return END;}
\;					{printf(";");}						//{return SMICOL;}
[ \t\n]*  			// IGNORA ESPAÇOS
.					{printf("Desconhecido ");}					//{return UNKNOW;}
%%
